'use strict'

const { PDFDocument, StandardFonts, rgb } = require('pdf-lib')
const fs = require('fs');
const chalk = require('chalk')
const config = require('./config.json')
const ripple = require('ripple-lib')
const api = new ripple.RippleAPI();
const account = api.generateAddress();
const name = process.argv[3] || "Wallet"

console.log(chalk.green('-----------------------------------------------'))
console.log(chalk.green('Ripple Wallet'), chalk.yellow('Generate Wallet'))
console.log(chalk.green('-----------------------------------------------'), '\n')

console.log('  Public address:', chalk.yellow(account.address))
console.log('  Wallet secret:', chalk.yellow(account.secret), '\n')

console.log(chalk.red('  Print this wallet and make sure to store it somewhere safe!'), '\n')
console.log(`  Note: You need to put at least ${config.baseReserve} ${config.currency} on this key for it to be an active account\n`)

async function create_pdf(pubkey, privkey, name) {
    const pdfDoc = await PDFDocument.create()
    const timesRomanFont = await pdfDoc.embedFont(StandardFonts.TimesRoman)
    const page = pdfDoc.addPage()
    const { width, height } = page.getSize()
    const fontSize = 20
    page.drawText("Private Key:"+privkey, {
        x: 80,
        y: height - 30 * fontSize,
        size: fontSize,
        font: timesRomanFont,
        })
    page.drawText(name, {
        x: 80,
        y: height - 4 * fontSize,
        size: fontSize,
        font: timesRomanFont,
        })
    page.drawText("Public Key:"+pubkey, {
    x: 70,
    y: height - 10 * fontSize,
    size: fontSize,
    font: timesRomanFont,
    })
    fs.writeFileSync(name + '.pdf', await pdfDoc.save());
}

create_pdf(account.address, account.secret, name)
console.log("  Creating pdf was successful")
